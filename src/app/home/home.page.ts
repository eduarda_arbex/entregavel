import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  public items = [
    {
      src: 'https://uploads.brasiljunior.org.br/uploads/ej/image/113/logo_azul.png',
    }
  ];
}
