import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { NotaComponent } from 'src/app/members/nota/nota.component';

@Component({
  selector: 'app-members',
  templateUrl: './members.page.html',
  styleUrls: ['./members.page.scss'],
})
export class MembersPage implements OnInit {

  public alunos = [
    {
      title: 'Alexandre Guimarães',
    },
    {
      title: 'Andressa Braz',
    },
    {
      title: 'Arbex',
    },
    {
      title: 'Campos Bello',
    },
    {
      title: ' Edgard Guitton',
    },
    {
      title: 'Hamilko',
    },
    {
      title: 'Luiz Guilherme',
    },
    {
      title: 'Orsi',
    },
    {
      title: 'Vinicius',
    }
  ];

  constructor(
    private router: Router,
    private modalCtrl: ModalController
  ) { }

  ngOnInit() {
  }

  onEnterNota() {
    this.modalCtrl
    .create({ component: NotaComponent })
    .then(modalEl => {
      modalEl.present();
    });
  }

}
