import { Component, OnInit } from '@angular/core';

import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {


  constructor(private authenticationService: AuthenticationService, private router: Router) { }

  ngOnInit() {
  }

  onLogin() {
    this.authenticationService.login();
    this.router.navigateByUrl('/members');
  }

  onSubmit(form: NgForm) {
    if (!form.valid) {
       return;
     }
    const email = form.value.email;
    const password = form.value.password;
    console.log(email, password);
  }

}
